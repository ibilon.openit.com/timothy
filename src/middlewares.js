const load = require('./lib/load')

function main(app) {
    let middlewares = Object.values(load(__dirname + '/middlewares'))
    middlewares.forEach(middleware => middleware(app))
}

module.exports = main