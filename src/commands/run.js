const assert = require('assert')
const fs     = require('fs')
const spawn  = require('./../lib/spawn')
const getopt = require('./../lib/getopt')
const pack   = require('./../lib/pack')
const upload = require('./../lib/upload')
const mktest = require('./../lib/mktest')

function main(argv) {
    let args = getargs(argv)
    if (args.remote)
        args.tests.forEach(test => remote(test, args.tags))
    else
        args.tests.forEach(run)
}

function getargs(argv) {
    let args = {
        remote : false,
        tags   : [],
        tests  : []
    }
    getopt(argv, function(option, value) {
        switch(option) {
            case 'remote':
                args.remote = true
                return false
            case 'tags':
                if (value)
                    args.tags = args.tags.concat(value.split(','))
                return true
            default:
                args.tests.push(value)
        }
    })
    return args
}

function run(path) {
    assert(fs.existsSync(path), `Test does not exist: ${path}`)
    process.chdir(path)
    if (!fs.existsSync('node_modules'))
        spawn('npm', 'install')
    let result = spawn('npm', 'test')
    if (result.status != 0)
        process.exit(1)
}

async function remote(path, tags) {
    let package = await pack(path)
    let object  = await upload(package)
    let test    = await mktest(object, tags)
    fs.rmSync(package)
    console.log(test.id)
}

module.exports = main