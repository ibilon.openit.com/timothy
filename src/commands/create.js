const assert = require('assert')
const fs     = require('fs-extra')
const spawn  = require('./../lib/spawn')

function main(argv) {
    let name        = argv[0]
    let boilerplate = `${__dirname}/../../boilerplate`
    assert(name, 'No test name')
    assert(!fs.existsSync(name), `Test already exists: ${name}`)
    console.log(`Creating test: ${name}`)
    fs.copySync(boilerplate, name)
}

module.exports = main