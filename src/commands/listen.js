const os        = require('os')
const fs        = require('fs')
const { join }  = require('path')
const interrupt = require('./../lib/interrupt-client')
const server    = require('./../lib/server')
const canonical = require('./../lib/canonical')
const fetch     = require('./../lib/fetch-json')
const pull      = require('./../lib/pull')
const spawn     = require('./../lib/spawn-merge')
const upload    = require('./../lib/upload-result')
const del       = require('./../lib/delete')
const hostname  = canonical.hostname()

async function main() {
    process.env.FORCE_COLOR = true
    let tmp = join(os.tmpdir(), 'timothy')
    fs.mkdirSync(tmp, { recursive: true })
    process.chdir(tmp)
    interrupt.init({ url: server.url() })
    interrupt.listen('runner', hostname, 'test').auto(run)
}

async function run() {
    let tests = await fetch(`/runner/${hostname}/test`)
    tests     = tests.sort((a, b) => a.created_at - b.created_at)
    for (let test of tests) {
        try {
            var path   = await pull(test.id)
            let result = await spawn('timothy', 'run', path)
            await upload(test.id, result)
        } catch (e) {
            // ...
        } finally {
            fs.rmSync(path, { force: true, recursive: true })
            try { await del(`/runner/${hostname}/test/${test.id}`) } catch (e) {}
        }
    }
}

module.exports = main