const { spawn } = require('child_process')
const canonical = require('./../lib/canonical')
const post      = require('./../lib/post')
const getopt    = require('./../lib/getopt')

async function main(argv) {
    let args  = getargs(argv)
    let data  = runner()
    data.tags = data.tags.concat(args.tags)
    await post('/runner', data)
    spawn('tim', ['listen'], { shell: true, stdio: 'ignore', detached: true, windowsHide: true }).unref()
}

function getargs(argv) {
    let args = { tags: [] }
    getopt(argv, function(option, value) {
        switch(option) {
            case 'tags':
                args.tags = value.split(',')
                break
        }
    })
    return args
}

function runner() {
    return {
        id   : canonical.hostname(),
        tags : [ canonical.hostname(), canonical.platform(), canonical.arch() ]
    }
}

module.exports = main