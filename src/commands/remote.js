const assert       = require('assert')
const localstorage = require('./../lib/localstorage')

function main(argv) {
    let server = argv[0]
    if (server)
        localstorage.set('server', server)
    else
        server = localstorage.get('server')
    if (server)
        console.log(server)
}

module.exports = main