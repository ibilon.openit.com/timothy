const colors  = require('colors')
const getopt  = require('./../lib/getopt')
const fetch   = require('./../lib/fetch-json')
let   runners = null

async function main(argv) {
    let args = getargs(argv)
    runners  = hash(await fetch('/runner'))
    for (let test_id of args.tests) {
        console.log(test_id)
        let results = await fetch(`/test/${test_id}/result`)
        results.forEach(print)
    }
}

function getargs(argv) {
    let args = {
        tests: []
    }
    getopt(argv, function(option, value) {
        switch(option) {
            default:
                args.tests.push(value)
        }
    })
    return args
}

function print(result) {
    let score = result.status == 0 ? 'PASS'.green : 'FAIL'.red
    let runner = runners[result.runner_id]
    console.log(score, ...runner.tags)
    if (result.status != 0)
        console.log(decode(result.stdio))
}

function hash(a) {
    let h = {}
    a.forEach(v => h[v.id] = v)
    return h
}

function decode(base64) {
    return Buffer.from(base64, 'base64').toString()
}

module.exports = main