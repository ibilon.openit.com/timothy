const fetch = require('./../lib/fetch')

async function main(argv) {
    let res     = await fetch('/runner')
    let runners = await res.json()
    runners.filter(runner => runner.active).forEach(runner => console.log(...runner.tags))
}

module.exports = main