const os      = require('os')
const mapping = {
    'win32': 'windows',
    'ia32' : '32-bit',
    'x64'  : '64-bit'
}

function hostname() {
    return os.hostname().toLowerCase()
}

function platform() {
    return resolve(os.platform())
}

function arch() {
    return resolve(os.arch())
}

function resolve(name) {
    return name in mapping ? mapping[name] : name
}

module.exports = { hostname, platform, arch }