const { join }          = require('path')
const fs                = require('fs')
const JSON_EXTENSION    = '.json'
let options             = { path: null }

function init(path) {
    options.path = path
    mkdir(options.path)
}

function mkdir(path) {
    if (!fs.existsSync(path))
        fs.mkdirSync(path, { recursive: true })
    return path
}

function get(key) {
    let path = resolve(key)
    if (exists(path))
        return read(path)
    return undefined
}

function set(key, data) {
    let path = resolve(key)
    write(path, data)
    return data
}

function resolve(key) {
    return join(options.path, key + JSON_EXTENSION)
}

function exists(path) {
    return fs.existsSync(path)
}

function read(path) {
    return JSON.parse(fs.readFileSync(path)).data
}

function write(path, data) {
    let json = { data }
    fs.writeFileSync(path, JSON.stringify(json))
}

module.exports = { init, get, set }