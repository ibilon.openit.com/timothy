const fetch    = require('./fetch-json')
const download = require('./download-test')

async function pull(id) {
    return await download(await fetch(`/test/${id}`))
}

module.exports = pull