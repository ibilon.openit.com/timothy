const assert                     = require('assert')
const { join, dirname, extname } = require('path')
const fs                         = require('fs')
const EventEmitter               = require('events')
const events                     = new EventEmitter()
let   options                    = { express: null }

function init(app) {
    options.express = app
    assert(options.express)
}

function listen(...names) {
    let app     = options.express
    let path    = resolve(names)
    let sockets = []

    function middleware(req, res, next) {
        res.once('finish', function() {
            if (res.statusCode == 200)
                interrupt(sockets)
        })
        next()
    }

    events.on('force', function(url) {
        interrupt(sockets, url)
    })

    app.post(path, middleware)
    app.delete(path, middleware)
    app.ws(path, function(ws, req) {
        ws.url    = req.url
        let index = sockets.length
        sockets.push(ws)
        ws.once('close', function() {
            sockets = remove(index, sockets)
        })
    })
}

function resolve(names) {
    names = names.map(name => [name, `:${name}_id`]).flat()
    names.pop()
    return '/' + names.join('/')
}

function remove(index, sockets) {
    sockets[index] = undefined
    if (sockets.removed == undefined)
        sockets.removed = 0
    sockets.removed++
    return cleanup(sockets)
}

function cleanup(sockets) {
    if (sockets.removed == 1000) {
        sockets         = sockets.filter(socket => socket != undefined)
        sockets.removed = 0
    }
    return sockets
}

function interrupt(sockets, url) {
    sockets.forEach(function(socket) {
        if (socket != undefined)
            if (url == undefined || socket.url == url)
                socket.send(1)
    })
}

function url(names) {
    return '/' + names.join('/') + '/.websocket'
}

function force(...names) {
    events.emit('force', url(names))
}

module.exports = {
    init,
    listen,
    force
}