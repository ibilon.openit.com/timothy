const fetch = require('./fetch')

async function main(path) {
    await fetch(path, { method: 'DELETE' })
}

module.exports = main