const assert        = require('assert')
const { URL }       = require('url')
const EventEmitter  = require('events')
const WebSocket     = require('ws')
let options         = { url: null, wss: false }

function init(o) {
    Object.assign(options, o)
    assert(options.url)
    options.url = new URL(options.url)
    if (options.wss)
        options.url.protocol = 'wss'
    else
        options.url.protocol = 'ws'
}

function listen(...names) {
    let path   = resolve(names)
    let events = new EventEmitter()

    function connect() {    
        let ws = new WebSocket(path)

        ws.on('open', function() {
            events.emit('interrupt')
        })

        ws.on('message', function() {
            events.emit('interrupt')
        })

        ws.on('close', function() {
            setTimeout(function() {
                connect()
            }, 60 * 1000)
        })
    }

    events.auto = function(fn) {
        let interrupt = false
        let running   = false

        function run() {
            running   = true
            interrupt = false
            setTimeout(async function() {
                try { await fn() } catch (e) {}
                running = false
                if (interrupt)
                    run()
            }, 0)
        }

        events.on('interrupt', function() {
            if (running)
                interrupt = true
            else
                run()
        })

        return events
    }

    connect()

    return events
}

function resolve(names) {
    return new URL(names.join('/'), options.url.href).href
}

module.exports = {
    init,
    listen
}