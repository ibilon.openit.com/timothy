const post      = require('./post')
const canonical = require('./canonical')

async function main(test_id, result) {
    return await post(`/test/${test_id}/result`, convert(result))
}

function convert(result) {
    return {
        id          : canonical.hostname(),
        runner_id   : canonical.hostname(),
        created_at  : +new Date(),
        status      : result.status,
        stdio       : result.stdio.toString('base64')
    }
}

module.exports = main