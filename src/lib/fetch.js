const fetch = require('node-fetch')
const server = require('./server')

function main(path, options) {
    return fetch(server.url() + path, options)
}

module.exports = main