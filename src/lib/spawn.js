const child_process = require('child_process')

function spawn(name, ...args) {
    let options = {
        shell: true,
        stdio: 'inherit'
    }
    return child_process.spawnSync(name, args, options)
}

module.exports = spawn