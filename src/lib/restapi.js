const assert                     = require('assert')
const { join, dirname, extname } = require('path')
const fs                         = require('fs')
const express                    = require('express')
const JSON_EXTENSION             = '.json'
let   options                    = { express: null, storage: null }

function init(a) {
    Object.assign(options, a)
    assert(options.express)
    assert(options.storage)
    mkdir(options.storage)
}

function location(...names) {
    let path     = resolve(names)
    let filepath = path + JSON_EXTENSION
    let index    = names.length % 2 == 0 ? false : true

    function get() {
        if (index) {
            if (!fs.existsSync(path))
                return []
            else
                return fs.readdirSync(path)
                         .filter(filename => extname(filename) == JSON_EXTENSION)
                         .map(filename => read(join(path, filename)))
        } else {
            assert(fs.existsSync(filepath), 'Resource does not exist')
            return read(filepath)
        }
    }

    function post(data) {
        assert(index, 'Invalid method')
        mkdir(path)
        if (Array.isArray(data)) {
            data.forEach(validate)
            data.forEach((data)=> write(path, data))
        } else {
            validate(data)
            write(path, data)
        }
        return data
    }

    function put(data) {
        assert(!index, 'Invalid method')
        let id = names[names.length - 1]
        assert(id === data.id, 'Invalid id')
        write(dirname(path), data)
        return data
    }

    function DELETE() {
        assert(!index, 'Invalid method')
        if (fs.existsSync(filepath))
            fs.rmSync(filepath)
    }

    return { get, post, put, delete: DELETE }
}

function url(url) {
    assert(url[0] == '/', 'Invalid URL')
    return location(...url.split('/').slice(1))
}

function create(parent, name) {
    let app     = options.express
    let path    = `${parent}/${name}`
    let path_id = `${path}/:${name}_id`

    app.get(path,       (req, res) => res.json(url(req.url).get()))
    app.get(path_id,    (req, res) => res.json(url(req.url).get()))
    app.post(path,      (req, res) => res.json(url(req.url).post(req.body)))
    app.put(path_id,    (req, res) => res.json(url(req.url).put(req.body)))
    app.delete(path_id, (req, res) => { url(req.url).delete(); res.end() })
    
    return { create: name => create(path_id, name)  }
}

function validate(data) {
    assert('id' in data               , 'id is missing')
    assert(typeof data.id == 'string' , 'id is not a string')
    assert(data.id != ''              , 'id is empty')
}

function resolve(names) {
    return join(options.storage, names.join('/').toLowerCase())
}

function read(path) {
    return JSON.parse(fs.readFileSync(path).toString())
}

function write(dir, data) {
    fs.writeFileSync(join(dir, data.id.toLowerCase() + JSON_EXTENSION), JSON.stringify(data))
}

function mkdir(path) {
    if (!fs.existsSync(path))
        fs.mkdirSync(path, { recursive: true })
    return path
}

module.exports = {
    init,
    create: name => create('', name),
    location
}