const fs                 = require('fs')
const { join, basename } = require('path')

function load(dir) {
    let files    = fs.readdirSync(dir)
    let paths    = files.map(file => join(dir, file))
    let requires = paths.map(path => require(path))
    return zip(files.map(file => basename(file, '.js')), requires)
}

function zip(a, b) {
    let hash = {}
    a.forEach((value, index) => hash[value] = b[index])
    return hash
}

module.exports = load