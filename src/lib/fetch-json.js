const fetch  = require('./fetch')
const server = require('./server')

async function main(path, options) {
    let response = await fetch(path, options)
    return response.json()
}

module.exports = main