const { createWriteStream } = require('fs')
const { pipeline }          = require('stream')
const { promisify }         = require('util')
const { join }              = require('path')
const fetch                 = require('node-fetch')
const contentDisposition    = require('content-disposition')
const mktemp                = require('mktemp')
const streamPipeline        = promisify(pipeline)

async function download(url) {
    let res  = await fetch(url)
    let name = contentDisposition.parse(res.headers.get('content-disposition')).parameters.filename
    let path = mktemp.createFileSync('XXXXXXX')
    await streamPipeline(res.body, createWriteStream(path))
    return { path, name }
}

module.exports = download