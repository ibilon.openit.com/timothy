const upload = require('./objectstore-upload')
const server = require('./server')

function main(filepath) {
    return upload(server.url() + '/object', filepath)
}

module.exports = main