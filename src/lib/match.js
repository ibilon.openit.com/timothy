function match(runner_tags, test_tags) {
    if (test_tags.length == 0)
        return true
    let filtered = runner_tags.filter(tag => ~test_tags.indexOf(tag))
    return filtered.length == test_tags.length
}

module.exports = match