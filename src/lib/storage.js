const package = require('../../package.json')
const os      = require('os')
const fs      = require('fs')

function init() {
    let dir = path()
    if (!fs.existsSync(dir))
        fs.mkdirSync(dir)
}

function path() {
    return `${os.homedir()}/.${package.name}`
}

module.exports = { init, path }