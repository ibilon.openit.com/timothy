const download = require('./download')
const server   = require('./server')

function main(id) {
    return download(server.url() + `/object/${id}`)
}

module.exports = main