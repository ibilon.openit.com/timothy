const { dirname, basename, join } = require('path')
const targz                       = require('targz')

function unpack(src) {
    return new Promise(function(resolve, reject) {
        let dest = join(dirname(src), basename(src, '.tar.gz'))
        targz.decompress({
            src,
            dest
        }, function(err) {
            if (err)
                return reject(err)
            return resolve(dest)
        })
    })
}

module.exports = unpack