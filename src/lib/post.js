const fetch = require('./fetch')

async function post(path, json) {
    let method  = 'POST'
    let body    = JSON.stringify(json)
    let headers = { 'Content-Type': 'application/json' }
    let res     = await fetch(path, { method, body, headers })
    return await res.json()
}

module.exports = post