const fs        = require('fs')
const fetch     = require('node-fetch')
const FormData  = require('form-data')

async function upload(url, filepath) {
    let form = new FormData()
    form.append('file', fs.createReadStream(filepath))
    let res = await fetch(url, { method: 'POST', body: form })
    return await res.json()
}

module.exports = upload