const assert                     = require('assert')
const { join, dirname, extname } = require('path')
const fs                         = require('fs')
const express                    = require('express')
const formidable                 = require('formidable')
const restapi                    = require('./restapi')
let   formidable_options         = { hash: 'sha1', multiples: true }
let   options                    = { express: null, storage: null }

function init(a, b = {}) {
    Object.assign(options, a)
    Object.assign(formidable_options, b)
    assert(options.express)
    assert(options.storage)
    mkdir(options.storage)
}

function location(...names) {
    let path  = resolve(names)
    let api   = restapi.location(...names)
    let index = names.length % 2 == 0 ? false : true

    function get() {
        let data = api.get()
        if (!index)
            data.path = path
        return data
    }

    function post(file) {
        assert(index, 'Invalid method')
        mkdir(path)
        let data = {}
        if (Array.isArray(file)) {
            file.forEach(file => move(file, path))
            data = file.map(convert)
        } else {
            move(file, path)
            data = convert(file)
        }
        return api.post(data)
    }

    function DELETE() {
        assert(!index, 'Invalid method')
        api.delete()
        if (fs.existsSync(path))
            fs.rmSync(path)
    }

    return { get, post, delete: DELETE }
}

function url(url) {
    assert(url[0] == '/', 'Invalid URL')
    return location(...url.split('/').slice(1))
}

function create(parent, name) {
    let app     = options.express
    let path    = `${parent}/${name}`
    let path_id = `${path}/:${name}_id`

    app.get(path,              (req, res) => res.json(url(req.url).get()))
    app.post(path, middleware, (req, res) => res.json(url(req.url).post(req.file)) )
    app.get(path_id,           (req, res) => { let file = url(req.url).get(); res.download(file.path, file.name) })
    app.delete(path_id,        (req, res) => { url(req.url).delete(); res.end() })
    
    return { create: name => create(path_id, name)  }
}

function middleware(req, res, next) {
    let form = formidable(formidable_options)
    form.parse(req, function(err, fields, files) {
        if (err)
            return next(err)
        try {
            assert('file' in files, 'No file')
            req.file = files.file
            next()
        } catch (e) {
            next(e)
        }
    })
}

function resolve(names) {
    return join(options.storage, names.join('/').toLowerCase())
}

function move(file, path) {
    fs.renameSync(file.path, join(path, file.hash))
}

function convert(file) {
    return {
        id        : file.hash,
        name      : file.name,
        size      : file.size,
        type      : file.type,
        created_at: +new Date()
    }
}

function mkdir(path) {
    if (!fs.existsSync(path))
        fs.mkdirSync(path, { recursive: true })
    return path
}

module.exports = {
    init,
    create: name => create('', name),
    location
}