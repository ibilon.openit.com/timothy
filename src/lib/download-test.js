const fs       = require('fs')
const { join } = require('path')
const download = require('./download-object')
const server   = require('./server')
const unpack   = require('./unpack')

async function main(test) {
    let file = await download(test.object_id)
    let a    = join('.', file.name)
    fs.renameSync(file.path, a)
    let b    = await unpack(a)
    fs.unlinkSync(a)
    return b
}

module.exports = main