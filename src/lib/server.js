const settings      = require('./../../settings.json')
const localstorage  = require('./localstorage')

function url() {
    let server  = localstorage.get('server')
    let port    = settings.port
    if (server)
        return `http://${server}:${port}`
    return undefined
}

module.exports = { url }