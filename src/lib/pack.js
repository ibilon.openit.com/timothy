const targz = require('targz')

function pack(src) {
    return new Promise(function(resolve, reject) {
        let dest = src + '.tar.gz'
        targz.compress({
            src,
            dest,
            tar: {
                ignore: function(path) {
                    return ~path.indexOf('node_modules')
                }
            }
        }, function(err) {
            if (err)
                return reject(err)
            return resolve(dest)
        })
    })
}

module.exports = pack