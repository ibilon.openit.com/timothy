const hyperid = require('hyperid')
const post    = require('./post')

function mktest(object, tags) {
    var test = {
        id         : hyperid().uuid,
        object_id  : object.id,
        created_at : +new Date(),
        tags
    }
    return post('/test', test)
}

module.exports = mktest