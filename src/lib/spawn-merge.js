const child_process = require('child_process')
const MemoryStream  = require('memorystream')

async function spawn(name, ...args) {
    return new Promise(function(resolve) {
        let stream = new MemoryStream()
        let child  = child_process.spawn(name, args, { shell: true })
        child.stdout.pipe(stream)
        child.stderr.pipe(stream)
        child.on('close', function(status) {
            resolve({ status, stdio: stream.read() })
        })
    })
}

module.exports = spawn