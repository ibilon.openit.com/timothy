const restapi   = require('./../lib/restapi')
const match     = require('./../lib/match')
const interrupt = require('./../lib/interrupt')

function main(app) {
    app.post('/test', function(req, res, next) {
        res.once('finish', function() {
            if (res.statusCode == 200)
                broadcast(req.body)
        })
        next()
    })
}

function broadcast(test) {
    let runners = restapi.location('runner').get()
    runners     = runners.filter(runner => runner.active).filter(runner => match(runner.tags, test.tags))
    runners.forEach(runner => push(test, runner))
    runners.forEach(runner => notify(runner))
}

function push(test, runner) {
    restapi.location('runner', runner.id, 'test').post({ id: test.id, created_at: +new Date() })
}

function notify(runner) {
    interrupt.force('runner', runner.id, 'test')
}

module.exports = main