const restapi = require('./../lib/restapi')

function main(app) {
    inactive_all()
    app.ws('/runner/:runner_id/test', function(ws, req, next) {
        let { runner_id } = req.params
        active(runner_id)
        ws.once('close', function() {
            inactive(runner_id)
        })
        next()
    })
}

function inactive_all() {
    let resource = restapi.location('runner')
    resource.post(resource.get().map(runner => set(runner, false)))
}

function active(runner_id, active = true) {
    let resource = restapi.location('runner', runner_id)
    resource.put(set(resource.get(), active))
}

function inactive(runner_id) {
    return active(runner_id, false)
}

function set(runner, active) {
    runner.active = active
    return runner
}

module.exports = main