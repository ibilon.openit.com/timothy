const express     = require('express')
const ws          = require('express-ws')
const package     = require('./../package.json')
const settings    = require('./../settings.json')
const storage     = require('./lib/storage')
const restapi     = require('./lib/restapi')
const objectstore = require('./lib/objectstore')
const interrupt   = require('./lib/interrupt')
const middlewares = require('./middlewares')

function main(argv) {
    let app = express()
    app.use(express.json({
        limit: '128mb'
    }))
    app.get('/', function(req, res) {
        res.json(package)
    })
    ws(app)
    interrupt.init(app)
    restapi.init({ express: app, storage: storage.path() + '/restapi' })
    objectstore.init({ express: app, storage: storage.path() + '/objectstore' })
    middlewares(app)
    interrupt.listen('runner', 'test')
    interrupt.listen('test')
    restapi.create('test').create('result')
    restapi.create('runner').create('test')
    objectstore.create('object')
    app.use(function(e, req, res, next) {
        res.status(400).json({
            name: e.name,
            message: e.message
        })
    })
    app.listen(settings.port, function() {
        console.log('Listening on port:', settings.port)
    })
}

module.exports = main