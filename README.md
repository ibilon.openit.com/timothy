# timothy

## Requirements

Install [NodeJS](https://nodejs.org/en/download/).

## Install

```sh
npm install -g https://gitlab.com/ibilon.openit.com/timothy.git
```

To verify.

```sh
timothy -V
```

## Create a test

Let us create a _sample_ test.

```sh
timothy create sample
```

This creates `sample` directory in your current working directory. This is called a "test package."

To run the test package.

```sh
timothy run sample
```

Edit the test in `sample/test/main.js`.

> **Info:** `timothy` is based on [Mocha](https://mochajs.org/) and [Chai](https://www.chaijs.com/) so you kindly need to 
learn how to use them.