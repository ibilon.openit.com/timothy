#!/usr/bin/env node
const package       = require('./package.json')
const { join }      = require('path')
const getopt        = require('./src/lib/getopt')
const storage       = require('./src/lib/storage')
const localstorage  = require('./src/lib/localstorage')
const commands      = require('./src/commands')
const argv          = process.argv.slice(2)

function main() {
    storage.init()
    localstorage.init(join(storage.path(), 'localstorage'))
    command() || args()
}

function args() {
    getopt(argv, function(option, value) {
        switch (option) {
            case 'V':
            case 'version':
                console.log(package.version)
                process.exit()
            case 'help':
                help()
                process.exit()
            default:
                console.error(`ERROR: Unknown option`)
                process.exit(1)
        }
    })
    help()
}

function help() {
    var msg = [
        `Usage: ${package.name} [command] [args...]`,
        'Commands:',
        Object.keys(commands).map(tab),
        '',
        `${package.name} - ${package.description}`
    ]
    console.log(msg.flat().join('\n'))
}

function command() {
    var name = argv[0]
    if (name in commands) {
        try {
            let command = commands[name]
            command(argv.slice(1))
            return true
        } catch (e) {
            console.error('ERROR:', e.message)
            if (process.env.DEBUG == 'true')
                throw e
            process.exit(1)
        }
    }
    return false
}

function tab(str) {
    return '    ' + str
}

main()